;; Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the MIT/X11 license.

;; You should have received a copy of the MIT/X11 license along with
;; this program. If not, see
;; <http://www.opensource.org/licenses/mit-license.php>.

(package (wak-common (0 1 0))
  
  (synopsis "common infrastructure for the Wak ports")
  (description
   "This package contains common infrastructure that is needed"
   "by all, or most, packages produced by the Wak porting effort."
   "Other packages should not use it and not depend on it.")
  (homepage "http://home.gna.org/wak/")
  
  (libraries
   ("private" -> ("wak" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
